package services

import (
	"fmt"
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/dao"
	"gitee.com/hubula/waomao/datasource"
	"gitee.com/hubula/waomao/models"
	"gitee.com/hubula/waomao/web/viewsmodels"
	"strconv"
	"strings"
)

type MemberService interface {
	// GetAll 列表查询
	GetAll(sqlwhere *common.SqlWhere) (*common.SqlReturn, error)
	// GetById 获取单条记录
	GetById(id int64) (*models.Member, error)
	GetByUnumber(id int64) (*models.Member, error)
	GetByMobile(mobile string) (*models.Member, error)
	// CountAll 统计
	CountAll() int64

	// Create 添加单条记录
	Create(data *models.Member) (int64, error)
	// Update 修改单条记录
	Update(data *models.Member, columns []string) (int64, error)
	// RuanDelete 软删除单条记录
	RuanDelete(id int64) (int64, error)
	// Delete 删除单条记录
	Delete(id int64) (int64, error)

	// GetWhere Sql语句
	GetWhere(sql string) []models.Member

	Auth(account, password string) (*viewsmodels.MemberCookie, error)
	Login(account, password string) (admUser *models.Member, err error)
	GetMemberByUserName(account string) (*models.Member, error)
}

type memberService struct {
	dao *dao.MemberDao
}

func NewMemberService() MemberService {
	return &memberService{
		dao: dao.NewMemberDao(datasource.MasterEngine()),
	}
}

func (s *memberService) GetAll(sqlwhere *common.SqlWhere) (*common.SqlReturn, error) {
	return s.dao.GetAll(sqlwhere)
}

// GetById 获取单条记录
func (s *memberService) GetById(id int64) (*models.Member, error) {
	return s.dao.GetById(id)
}

func (s *memberService) GetByUnumber(id int64) (*models.Member, error) {
	return s.dao.GetByUnumber(id)
}

func (s *memberService) GetByMobile(mobile string) (*models.Member, error) {
	return s.dao.GetByMobile(mobile)
}

// CountAll 统计
func (s *memberService) CountAll() int64 {
	return s.dao.CountAll()
}

// Create 添加单条记录
func (s *memberService) Create(data *models.Member) (int64, error) {
	// 先更新缓存
	//s.updateByCache(data, nil)
	// 再更新数据库
	return s.dao.Create(data)
}

// Update 修改单条记录
func (s *memberService) Update(data *models.Member, columns []string) (int64, error) {
	// 先更新缓存
	//s.updateByCache(data, columns)
	// 再更新数据库
	return s.dao.Update(data, columns)
}

// RuanDelete 软删除单条记录
func (s *memberService) RuanDelete(id int64) (int64, error) {
	// 先更新缓存
	//data := &models.Users{Id: id}
	//s.updateByCache(data, nil)
	// 再更新数据库
	return s.dao.RuanDelete(id)
}

// Delete 删除单条记录
func (s *memberService) Delete(id int64) (int64, error) {
	// 先更新缓存
	//data := &models.Users{Id: id}
	//s.updateByCache(data, nil)
	// 再更新数据库
	return s.dao.Delete(id)
}

// GetWhere Sql语句
func (s *memberService) GetWhere(sql string) []models.Member {
	return s.dao.GetWhere(sql)
}

//登录验证
func (c *memberService) Auth(account, password string) (*viewsmodels.MemberCookie, error) {
	//登录验证
	admUser, err := c.Login(account, password)
	//登录成功
	if err == nil {
		//转换为session
		Session := common.ConvertCookie(admUser)
		return Session, nil
	}
	//fmt.Println(err)
	return nil, err
}

//登录验证
func (c *memberService) Login(account, password string) (admUser *models.Member, err error) {
	if len(account) == 0 {
		return nil, fmt.Errorf("账号 不能为空")
	}
	if len(password) == 0 {
		return nil, fmt.Errorf("密码 不能为空")
	}
	admUser, err = c.GetMemberByMobile(account)
	if err == nil {
		if admUser.Uid != 0 {
			password := common.PasswordSalt(password, admUser.Salt)

			if !strings.EqualFold(password, admUser.Password) {
				return nil, fmt.Errorf("密码 错误")
			}
			return admUser, nil
		} else {
			return nil, fmt.Errorf("账号 不存在")
		}
	}
	return nil, fmt.Errorf("登陆失败，请稍后重试..")
}

//根据用户名查找
func (c *memberService) GetMemberByUserName(account string) (*models.Member, error) {
	yhaoma, _ := strconv.Atoi(account)
	mod, err := c.GetByUnumber(int64(yhaoma))
	//ok, err := o.Where("username=?", account).Get(mod)
	if err != nil {
		return nil, fmt.Errorf("用户不存在")
	}
	return mod, nil
}

func (c *memberService) GetMemberByMobile(mobile string) (*models.Member, error) {
	mod, err := c.GetByMobile(mobile)
	//ok, err := o.Where("username=?", account).Get(mod)
	if err != nil {
		return nil, fmt.Errorf("用户不存在")
	}
	return mod, nil
}
