package services

import (
	"fmt"
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/dao"
	"gitee.com/hubula/waomao/datasource"
	"gitee.com/hubula/waomao/models"
	"gitee.com/hubula/waomao/web/viewsmodels"
	"strconv"
	"strings"
)

type AdminService interface {
	// GetAll 列表查询
	GetAll(sqlwhere *common.SqlWhere) (*common.SqlReturn, error)
	// GetById 获取单条记录
	GetById(id int64) (*models.Admin, error)
	GetByAnumber(id int64) (*models.Admin, error)
	// CountAll 统计
	CountAll() int64

	// Create 添加单条记录
	Create(data *models.Admin) (int64, error)
	// Update 修改单条记录
	Update(data *models.Admin, columns []string) (int64, error)
	// RuanDelete 软删除单条记录
	RuanDelete(id int64) (int64, error)
	// Delete 删除单条记录
	Delete(id int64) (int64, error)

	// GetWhere Sql语句
	GetWhere(sql string) []models.Admin

	Auth(account, password string) (*viewsmodels.AdminSession, error)
	Login(account, password string) (admUser *models.Admin, err error)
	GetMemberByUserName(account string) (*models.Admin, error)
}

type adminService struct {
	dao *dao.AdminDao
}

func NewAdminService() AdminService {
	return &adminService{
		dao: dao.NewAdminDao(datasource.MasterEngine()),
	}
}

func (s *adminService) GetAll(sqlwhere *common.SqlWhere) (*common.SqlReturn, error) {
	return s.dao.GetAll(sqlwhere)
}

// GetById 获取单条记录
func (s *adminService) GetById(id int64) (*models.Admin, error) {
	return s.dao.GetById(id)
}

// GetById 获取单条记录
func (s *adminService) GetByAnumber(id int64) (*models.Admin, error) {
	return s.dao.GetByAnumber(id)
}

// CountAll 统计
func (s *adminService) CountAll() int64 {
	return s.dao.CountAll()
}

// Create 添加单条记录
func (s *adminService) Create(data *models.Admin) (int64, error) {
	// 先更新缓存
	//s.updateByCache(data, nil)
	// 再更新数据库
	return s.dao.Create(data)
}

// Update 修改单条记录
func (s *adminService) Update(data *models.Admin, columns []string) (int64, error) {
	// 先更新缓存
	//s.updateByCache(data, columns)
	// 再更新数据库
	return s.dao.Update(data, columns)
}

// RuanDelete 软删除单条记录
func (s *adminService) RuanDelete(id int64) (int64, error) {
	// 先更新缓存
	//data := &models.Users{Id: id}
	//s.updateByCache(data, nil)
	// 再更新数据库
	return s.dao.RuanDelete(id)
}

// Delete 删除单条记录
func (s *adminService) Delete(id int64) (int64, error) {
	// 先更新缓存
	//data := &models.Users{Id: id}
	//s.updateByCache(data, nil)
	// 再更新数据库
	return s.dao.Delete(id)
}

// GetWhere Sql语句
func (s *adminService) GetWhere(sql string) []models.Admin {
	return s.dao.GetWhere(sql)
}

//登录验证
func (c *adminService) Auth(account, password string) (*viewsmodels.AdminSession, error) {
	//登录验证
	admUser, err := c.Login(account, password)

	//登录成功
	if err == nil {
		//转换为session
		Session := common.ConvertSession(admUser)
		return Session, nil
	}
	//fmt.Println(err)
	return nil, err
}

//登录验证
func (c *adminService) Login(account, password string) (admUser *models.Admin, err error) {
	if len(account) == 0 {
		return nil, fmt.Errorf("账号 不能为空")
	}
	if len(password) == 0 {
		return nil, fmt.Errorf("密码 不能为空")
	}
	admUser, err = c.GetMemberByUserName(account)
	if err == nil {
		if admUser.Aid != 0 {
			password := common.PasswordSalt(password, admUser.Salt)

			if !strings.EqualFold(password, admUser.Password) {
				return nil, fmt.Errorf("密码 错误")
			}
			return admUser, nil
		} else {
			return nil, fmt.Errorf("账号 不存在")
		}
	}
	return nil, fmt.Errorf("登陆失败，请稍后重试..")
}

//根据用户名查找
func (c *adminService) GetMemberByUserName(account string) (*models.Admin, error) {
	yhaoma, _ := strconv.Atoi(account)
	mod, err := c.GetByAnumber(int64(yhaoma))
	//ok, err := o.Where("username=?", account).Get(mod)
	if err != nil {
		return nil, fmt.Errorf("用户不存在")
	}
	return mod, nil
}
