package conf

import "time"

//配置文件结构体

type ConfigToml struct {
	//全局信息
	Title string
	//开发阶段
	RunMode string

	App   app
	Stage map[string]stage `toml:"stage"`

	DB    map[string]db
	Redis map[string]redis

	Releases releases
	Company  company
	Song     []song
}

//应用信息
type app struct {
	//网站名字
	Name string
	//只有出现错误时使用
	SiteName string
	//http 或者 https
	HttpOrHttps string
	//组织 机构
	Org string `toml:"organization"`
	//网站作者  Owner  string
	Author string
	//时间
	Release time.Time
	//markdown 格式代码
	Mark string
}

//开发阶段 开发环境 dev 测试环境 生产环境 prod
type stage struct {
	//域名
	DomainName string
	//端口
	Port int
	//是否开启请求日志
	CustomLogger bool
	//默认用户 true 用于调试，不用每次登陆
	AdminLoad bool
	//芝麻开门
	CantKey string
	Cant    string
	//主题
	Theme string

	//网站Cookie Session
	//是否开启
	Sessionon bool
	//前缀
	SessionPrefix string
	//名字
	SessionName string
}

//数据库配置
// root:root@tcp(127.0.0.1:3306)/lottery?charset=utf-8
type db struct {
	//数据库类型，默认 MYSQL
	DriverName string
	//主机ip
	Host string
	//端口，默认 3306
	Port int
	//数据库登录帐号
	User string
	//登录密码
	Password string
	//数据库名
	Database string
	//向服务器请求连接所使用的字符集，默认：无
	Charset string
	//MySQL的时区设置
	Loc string
	//显示sql
	ShowSql bool
	//日志级别
	LogLevel string
	//最大空闲数
	MaxIdleConns int
	//最大连接数
	MaxOpenConns int
	//状态 是否启用
	IsRunning bool
	//查询结果是否自动解析为时间
	ParseTime bool
	////SetConnMaxLifetime(time.Second * 500) //设置连接超时500秒
	//ConnMaxLifetime int
	////是否启用 SSL 连接模式，默认：MySqlSslMode.None
	//SslMode         string
}

//Redis主从 字典对象
type redis struct {
	//主机ip
	Host string
	//端口
	Port int
}

//二维数组
type releases struct {
	Release []string
	Tags    [][]interface{}
}

//对象嵌套 公司信息
type company struct {
	Name   string
	Detail detail
}

type detail struct {
	Type string
	Addr string
	ICP  string
}

type song struct {
	Name string
	Dur  duration `toml:"duration"`
}

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}
