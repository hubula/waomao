package conf

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"path/filepath"
	"sync"
)

var (
	cfg     *ConfigToml
	once    sync.Once
	cfgLock = new(sync.RWMutex)
)

//var Rds = `
//# 全局信息
//title = "TOML格式配置文件示例"
//#开发阶段
//runmode = "dev"
//#密码干扰码
//password_salt ="gou8huo8le8wao8mao8hu8bu8la8lang8li"
//# AES-128。key长度：16, 24, 32 bytes 对应 AES-128, AES-192, AES-256
//aes_key="Hubu&La_LangLi_WaoMao"
//`

func TomlData() *ConfigToml {
	once.Do(ReloadConfig)
	cfgLock.RLock()
	defer cfgLock.RUnlock()
	return cfg
}

func ReloadConfig() {
	//读取文件
	filePath, err := filepath.Abs("./configs/confdata.toml")
	if err != nil {
		fmt.Println("load config error: ", err)
		panic(err)
	}
	//fmt.Printf("parse toml file once. filePath: %s\n", filePath)
	if _, err := toml.DecodeFile(filePath, &cfg); err != nil {
		fmt.Println("Para config failed: ", err)
		panic(err)
	}

	//读取字符串
	//if _, err := toml.Decode(Rds, &cfg); err != nil {
	//	panic(err)
	//}

	cfgLock.Lock()
	defer cfgLock.Unlock()
}

//fmt.Println(conf.ConfToml().Title)
