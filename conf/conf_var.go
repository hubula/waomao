package conf

//项目配置-变量

import (
	"github.com/kataras/iris/v12/middleware/logger"
	"github.com/kataras/iris/v12/sessions"
	"time"
)

var (
	//中国时区
	SysTimeLocation, _ = time.LoadLocation("Asia/Chongqing")

	// 请求日志记录
	CustomLogger = logger.New(logger.Config{
		//状态显示状态代码
		Status: true,
		// IP显示请求的远程地址
		IP: true,
		//方法显示http方法
		Method: true,
		// Path显示请求路径
		Path: true,
		// Query将url查询附加到Path。
		Query: true,
		//Columns：true，
		// 如果不为空然后它的内容来自`ctx.Values(),Get("logger_message")
		//将添加到日志中。
		MessageContextKeys: []string{"logger_message"},
		//如果不为空然后它的内容来自`ctx.GetHeader（“User-Agent”）
		MessageHeaderKeys: []string{"User-Agent"},
	})

	//http 或者 https
	HttpOrHttps = TomlData().App.HttpOrHttps
	//域名
	DomainName = TomlData().Stage[TomlData().RunMode].DomainName
	//首页
	HomePage  = HttpOrHttps + DomainName
	HomePages = HttpOrHttps + DomainName + "/"

	//开发阶段 默认用户
	AdminOpen = TomlData().Stage[TomlData().RunMode].AdminLoad

	GroupadminKey = TomlData().Stage[TomlData().RunMode].CantKey
	GroupadminVal = TomlData().Stage[TomlData().RunMode].Cant

	//是否需要启动全局计划任务服务
	RunningCrontabService = false

	Sess = sessions.New(sessions.Config{Cookie: CookieNameForSessionID})

	//SignSecret ObjSalesign 签名密钥
	SignSecret = []byte("0123456789abcdef")

	//CookieSecret cookie中的加密验证密钥
	CookieSecret = "hellolottery"
)
