package conf

type MuBanPath struct {
	//前端或者后台
	IndexOrAdmin string
	//模块
	PathModel string
	//页面
	PagePath string
}
