package datasource

import (
	"fmt"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/models"
	_ "github.com/go-sql-driver/mysql"
	"github.com/kataras/golog"
	"github.com/xormplus/xorm"
	"log"
	"sync"
	"xorm.io/core"
)

var (
	//dbLock 互斥锁
	dbLock sync.Mutex
	//保持一个
	masterEngine *xorm.Engine
	slaveEngine  *xorm.Engine
)

// GetConnURL 获取数据库连接的url
// true：master主库
func GetConnURL(mors string) (url string) {
	//"mysql", "user:password@tcp(127.0.0.1:3306)/dbname
	// ?charset=utf8&parseTime=True&loc=Local"
	url = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=%v&loc=%s",
		conf.TomlData().DB[mors].User,
		conf.TomlData().DB[mors].Password,
		conf.TomlData().DB[mors].Host,
		conf.TomlData().DB[mors].Port,
		conf.TomlData().DB[mors].Database,
		conf.TomlData().DB[mors].Charset,
		conf.TomlData().DB[mors].ParseTime,
		conf.TomlData().DB[mors].Loc)
	//golog.Infof("@@@ DB conn==>> %s", url)
	return
}

//InstanceDbMaster 单例的模式 得到唯一的主库实例
// 主库，单例 InstanceDbMaster MasterEngine

func MasterEngine() *xorm.Engine {
	//如果已经连接 就返回
	if masterEngine != nil {
		return masterEngine
	}
	//如果存在并发
	//加锁 创建之前锁定
	dbLock.Lock()
	//解锁
	defer dbLock.Unlock()

	//也许存在排队的可能
	if masterEngine != nil {
		return masterEngine
	}

	//连接数据库
	engine, err := xorm.NewEngine(conf.TomlData().DB["master"].DriverName, GetConnURL("master"))
	if err != nil {
		golog.Fatalf("@@@ Instance Master DB error!! %s", err)
		log.Fatal("dbhelper.InstanceDbMaster NewEngine error ", err)
		return nil
	}

	settings(engine, "master")

	engine.SetMapper(core.GonicMapper{})

	//返回实例
	masterEngine = engine
	return masterEngine
}

// SlaveEngine 从库，单例
func SlaveEngine() *xorm.Engine {
	//如果已经连接 就返回
	if slaveEngine != nil {
		return slaveEngine
	}
	//如果存在并发
	//加锁 创建之前锁定
	dbLock.Lock()
	//解锁
	defer dbLock.Unlock()
	//也许存在排队的可能
	if slaveEngine != nil {
		return slaveEngine
	}

	//连接数据库
	engine, err := xorm.NewEngine(conf.TomlData().DB["slave"].DriverName, GetConnURL("slave"))
	if err != nil {
		log.Fatal("dbhelper.InstanceDbMaster NewEngine error ", err)
		return nil
	}

	settings(engine, "slave")

	//返回实例
	slaveEngine = engine
	return engine
}

//数据库连接后的基本设置
func settings(engine *xorm.Engine, mors string) {
	//打印连接信息
	if err := engine.Ping(); err != nil {
		fmt.Println(err)
	}

	//同步创建数据表
	err := engine.Sync2(
		new(models.ADemo),
		new(models.Admin),
		new(models.Member))

	if err != nil {
		panic(err.Error())
	}

	//调试用的。展示每一条调试语句调试时间
	engine.ShowSQL(conf.TomlData().DB[mors].ShowSql)
	//engine.ShowSQL(false)

	//时区
	engine.SetTZLocation(conf.SysTimeLocation)

	if conf.TomlData().DB[mors].MaxIdleConns > 0 {
		engine.SetMaxIdleConns(conf.TomlData().DB[mors].MaxIdleConns)
	}
	if conf.TomlData().DB[mors].MaxOpenConns > 0 {
		engine.SetMaxOpenConns(conf.TomlData().DB[mors].MaxOpenConns)
	}

	//engine.SetMaxOpenConns(10)

	// 性能优化的时候才考虑，加上本机的SQL缓存
	//cacher := xorm.NewLRUCacher(xorm.NewMemoryStore(), 1000)
	//engine.SetDefaultCacher(cacher)
}
