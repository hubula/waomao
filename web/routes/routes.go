package routes

import (
	"gitee.com/hubula/waomao/bootstrap"
	"gitee.com/hubula/waomao/services"
	"gitee.com/hubula/waomao/web/controllers/backend/admin"
	"gitee.com/hubula/waomao/web/controllers/backend/groupadmin"
	"gitee.com/hubula/waomao/web/controllers/backend/groupadmin/gManager"
	"gitee.com/hubula/waomao/web/controllers/backend/groupadmin/gManager/gMMember"
	"gitee.com/hubula/waomao/web/controllers/fronted/demo"
	"gitee.com/hubula/waomao/web/controllers/fronted/demo/demoDiyTest"
	"gitee.com/hubula/waomao/web/controllers/fronted/demo/demoHtmlOrJq"
	"gitee.com/hubula/waomao/web/controllers/fronted/demo/demoMubanTest"
	"gitee.com/hubula/waomao/web/controllers/fronted/demo/demoMysqlTest"
	"gitee.com/hubula/waomao/web/controllers/fronted/passport"
	"gitee.com/hubula/waomao/web/controllers/fronted/passport/pMember"
	"gitee.com/hubula/waomao/web/controllers/fronted/www"
	"gitee.com/hubula/waomao/web/middleware"
	"gitee.com/hubula/waomao/web/middleware/identity"
	"github.com/kataras/iris/v12/mvc"
)

// Configure 和 bootstrap 里定义的一样
func Configure(b *bootstrap.Bootstrapper) {
	//主要是把indexcontrollers放进去 里面定义了很多service
	var (
		aDemoService  = services.NewADemoService()
		adminService  = services.NewAdminService()
		memberService = services.NewMemberService()
	)

	//---------------------------------------------------------------------
	//用mvc创建一个新的路径
	index := mvc.New(b.Party("/"))
	//把需要的 Service 注册进去
	index.Register(memberService)
	//路径发给Handle
	index.Handle(new(www.IndexController))

	//演示
	demoURL := index.Party("/demo")
	demoURL.Register(aDemoService)
	demoURL.Handle(new(demo.IndexController))
	demoURL.Controllers[0].GetRoute("Get").Name = "返回html页面"
	//demoURL.Controllers[0].GetRoute("GetInfo").Name = "返回json数据"
	//demoURL.Controllers[0].GetRoute("GetBoom").Name = "返回错误页面"

	//演示 - 模板布局
	dMubanTestURL := demoURL.Party("/mubantest")
	dMubanTestURL.Register(aDemoService)
	dMubanTestURL.Handle(new(demoMubanTest.IndexController))

	//演示 - 数据库操作
	dMysqlTestURL := demoURL.Party("/mysqltest")
	dMysqlTestURL.Handle(new(demoMysqlTest.IndexController))

	//演示 - html样式
	dHtmlOrJqURL := demoURL.Party("/htmlorjq")
	dHtmlOrJqURL.Register(aDemoService)
	dHtmlOrJqURL.Handle(new(demoHtmlOrJq.IndexController))

	//演示 - 自定义测试
	dDiyTestURL := demoURL.Party("/diytest")
	dDiyTestURL.Register(aDemoService)
	dDiyTestURL.Handle(new(demoDiyTest.IndexController))

	//二级域名示例
	demoErji := mvc.New(b.Party("demo."))
	demoIndex := demoErji.Party("/")
	demoIndex.Register(aDemoService)
	demoIndex.Handle(new(demo.IndexController))

	/*或者二级目录 用nginx进行解析
	demo. 的二级域名
	*/

	//---------------------------------------------------------------------

	//后台
	adminURL := index.Party("/admin")
	adminURL.Router.Use(middleware.BasicAuth)
	adminURL.Register(adminService)
	adminURL.Handle(new(admin.IndexController))

	//后台
	groupadminURL := index.Party("/groupadmin")
	groupadminURL.Router.Use(identity.AdminAuth)
	groupadminURL.Register(adminService)
	groupadminURL.Handle(new(groupadmin.IndexController))

	//后台首页
	gManagerURL := groupadminURL.Party("/manager")
	gManagerURL.Handle(new(gManager.IndexController))

	//管理员
	gMMemberURL := gManagerURL.Party("/member")
	gMMemberURL.Register(aDemoService)
	gMMemberURL.Handle(new(gMMember.IndexController))

	//博客后台
	//////gMBlobURL := gManagerURL.Party("/blog")
	//////gMBlobURL.Handle(new(groupadmin.ManagerBlogController))
	//////
	//////gMYangyanURL := gManagerURL.Party("/yangyan")
	//////gMYangyanURL.Register(adminService)
	//////gMYangyanURL.Handle(new(groupadmin.ManagerYangyanController))

	//前台-------------------------------------------------------------------

	//帐号
	passportURL := index.Party("/passport")
	passportURL.Register(memberService)
	passportURL.Handle(new(passport.IndexController))

	pMemberURL := passportURL.Party("/member")
	pMemberURL.Register(memberService)
	pMemberURL.Handle(new(pMember.IndexController))

	//假定错误
	//errorURL := mvc.New(b.Party("/error"))
	//errorURL.Register(aDemoService)
	//errorURL.Handle(new(errorDiy.IndexController))
	//
	////博客
	//blogURL := mvc.New(b.Party("/blog"))
	//blogURL.Register(aDemoService)
	//blogURL.Handle(new(blog.IndexController))
	//
	////养眼
	//yangyanURL := mvc.New(b.Party("/yangyan"))
	//yangyanURL.Register(aDemoService)
	//yangyanURL.Handle(new(yangyan.IndexController))
	//
	////订单
	////order := mvc.New(b.Party("/order"))
	////order.Register(userService)
	////order.Handle(new(fronted.OrderController))
	//
	////开放平台
	//openURL := mvc.New(b.Party("/open"))
	//openURL.Register(aDemoService)
	//openURL.Handle(new(open.IndexController))

}
