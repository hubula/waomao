
/**
 *把有连字符号的字符串转化为驼峰命名法的字符串
 */
function toCamelCase(value){
    return value.replace(/-(\w)/g,(matched,letter)=>{
        return letter.toUpperCase();
    });
}

/**
 告知浏览器支持的指定css属性情况
 key-css属性，是属性的名字，不需要加前缀
 return  -支持的属性情况 如果返回值是空，那么证明浏览器不支持该属性。
 */
function validateCssKey(key){
    const jsKey=toCamelCase(key);//有些css属性是连字符号形成
    if(jsKey in document.documentElement.style){
        return key;
    }
    let validKey='';
    //属性名为前缀在js中的形式，属性值是前缀在css中的形式
    //经尝试，Webkit也可是首字母小写webkit
    const prefixMap={
        Webkit:'-webkit-',
        Moz:'-moz-',
        ms:'-ms-',
        O:'-o-'
    };
    for (const jsPrefix in prefixMap){
        const styleKey=toCamelCase(`${jsPrefix}-${jsKey}`);
        if(styleKey in document.documentElement.style){
            validKey=prefixMap[jsPrefix]+key;
            break;
        }
    }
    return validKey;
}

/**
 * 检查浏览器是否支持某个css属性值
 * key - 检查的属性值所属的css属性名
 * value - 要检查的css属性值（不要带前缀）
 * return  - 返回浏览器支持的属性值 如果返回值是空，那么证明浏览器不支持该属性值
 */
function valiateCssValue (key, value) {
    var prefix = ['-o-', '-ms-', '','-moz-', '-webkit-'];
    var prefixValue = [];
    for (var i = 0; i < prefix.length; i++) {
        //向数组的末尾添加一个或多个元素，并返回新的长度
        //-o-sticky
        prefixValue.push(prefix[i] + value)
    }
    //通过指定名称创建一个元素
    var element = document.createElement('div');
    //获取元素 style 属性的值:
    var eleStyle = element.style;
    for (var j = 0; j < prefixValue.length; j++) {
        eleStyle[key] = prefixValue[j];
    }
    return eleStyle[key];
}

/**
 * 检查浏览器是否有支持的sticky值，没有返回false，有就添加sticky相关css，实现吸顶
 */
function validateSticky () {
    var supportStickyValue = valiateCssValue('position', 'sticky');
    if (supportStickyValue) {
        var navBarWrap = document.querySelector('#top02');
        navBarWrap.style.position = supportStickyValue;
        navBarWrap.style.top = '0px';
        return true;
    }
    return false;
}

//我们可以简单地上述对于两种形式的css兼容性进行合并，
// 不用显式地采用调用检查属性名还是属性值的方法，直接传入css，告知浏览器支持的情况。

function validCss (key, value) {
    const validCss = validateCssKey(key);
    if (validCss) {
        return validCss;
    }
    return valiateCssValue(key, value);
}

// //从当前页面的 地址的参数中 提取出指定的参数值
// function getQueryString(){
//     //如：假设一个网页的地址最后是 test.html?aaa=123&bbb=456$ccc=789
//     //在这个页面中调用该函数，当key的值是aaa时，函数返回123；key是bbb时，返回456...
//     //document.location.search
//     //上面这部分代码提取了前面页面地址中的参数列表，如：?aaa=123&bbb=456$ccc=789
//     //match(new RegExp("(?:^\\?|&)"+key+"=(.*?(?=#/&/$)"));
//     //match() 是一个字符串的方法，用来匹配相应的字符串
//     // 里面的参数是一个正则表达式，可以匹配传入函数的“key”的那段字符串，并提取出key后面的参数值，
//     // 存在value这个变量中。
//     // return(value?decodeURI(value[0]:null));
//     // 最后，返回结果。a?b:c是一个条件语句，在这里就是如果匹配到了key的值，就返回其值，否则返回null。
//
//     var result = location.search.match(new RegExp("[\?\&][^\?\&]+=[^\?\&]+","g"));
//     if(result == null){
//         return "";
//     }
//     for(var i = 0; i < result.length; i++){
//         result[i] = result[i].substring(1);
//     }
//     return result;
// }

//跳转页面添加参数
// function goSort(name,value){
//     //处理url 参数
//     var string_array = getQueryString();
//     //url indexOf("xuanze")==-1 不包含xuanze 加上xuanze
//     var oldUrl = (document.URL.indexOf("xuanze")==-1)?document.URL+"xuanze":document.URL;
//     var newUrl;
//     if(string_array.length>0)//如果已经有筛选条件
//     {  var repeatField = false;
//         for(var i=0;i<string_array.length;i++){
//             if(!(string_array[i].indexOf(name)==-1)){
//                 repeatField = true;//如果有重复筛选条件，替换条件值
//                 newUrl = oldUrl.replace(string_array[i],name+"="+value);
//             }
//         }
//         //如果没有重复的筛选字段 拼接
//         if(repeatField == false){
//             newUrl = oldUrl+"&"+name+"="+value;
//         }
//     }else{//如果还没有筛选条件 拼接
//         newUrl = oldUrl+"?"+name+"="+value;
//     }
//     //跳转
//     window.location = newUrl;
// }

//下拉导航显示 会出现抖动
// $('.nav>ul>li').hover(function(){
//     //     $(this).children('.sub').slideDown();
//     // },function () {
//     //     $(this).children('.sub').slideUp();
//
//     //slideToggle()就是切换方法，从鼠标离开切换到鼠标经过。
//     // 或者从鼠标经过切换到鼠标离开。 这种方法更简单，但比较难理解一些。
//     //stop是停止上一次的动画，防止鼠标快速经过时，上个动画还在执行，产生抖动
//     $(this).children('.sub').stop().slideToggle();
// });

//下拉导航显示 完美
// // 线程 IDs
// var mouseover_tid = [];
// var mouseout_tid = [];
//
// jQuery("nav>ul>li").each(function(index){
//     jQuery(this).hover(
//         // 鼠标进入
//         function(){
//             var _self = this;
//             // 停止卷起事件
//             clearTimeout(mouseout_tid[index]);
//             // 当鼠标进入超过 0.2 秒, 展开菜单, 并记录到线程 ID 中
//             mouseover_tid[index] = setTimeout(function() {
//                 //$(this).find("a").addClass("current");
//                 jQuery(_self).children('.sub').slideDown(300);
//             }, 201);
//         },
//
//         // 鼠标离开
//         function(){
//             var _self = this;
//             // 停止展开事件
//             clearTimeout(mouseover_tid[index]);
//             // 当鼠标离开超过 0.2 秒, 卷起菜单, 并记录到线程 ID 中
//             mouseout_tid[index] = setTimeout(function() {
//                 //$(this).find("a").removeClass("current");
//                 jQuery(_self).children('.sub').slideUp(300);
//             }, 201);
//         }
//     );
// });


// 给页面加入scroll 滚动事件
// $(window).scroll(function(){
//     //控制导航
//     if($(window).scrollTop()<100){
//         $('#headdiv').stop().animate({"top":"0px"},1000);
//         $('.top02').css("height","95px");
//         $('.logo').css("padding-top","20px");
//         $('.nav .mmm').css("padding-top","50px");
//         $('.nav .sub').css("top","125px");
//
//     }else{
//         $('#headdiv').stop().animate({"top":"-30px"},1000);
//         $('.top02').css("height","75px");
//         $('.logo').css("padding-top","3px");
//         $('.nav .mmm').css("padding-top","30px");
//         $('.nav .sub').css("top","75px");
//     }
// });

//导航高亮 trim() 方法用于删除字符串的头尾空格。
// var lochref = $.trim(window.location.href);// 获得当前页面的URL
// $(".headdiv .top02 .top02_center .navdiv ul li a").each(function (i) {
//     //获取导航栏中每个a标签
//     var me = $(this);
//     var mehref = $.trim(me.get(0).href);//获得每个<a>的url
//     /*判断当前url是否包含每个导航特定的rel  lochref == mehref是为了专门判断首页  */
//     if (lochref.indexOf(me.parent().attr('rel')) !== -1 || lochref === mehref) {
//         //parent() 上一级div
//         //me.parent().addClass("cur");
//         me.addClass("cur");
//     } else {
//         //parent() 上一级div
//         //me.parent().removeClass("cur");
//         me.removeClass("cur");
//     }
// });


//根据网站目录结构对应导航高亮显示（可忽略）
// var url = window.location.href.toLowerCase();
// //alert(url);
// if (url.indexOf("/website/case/") > -1) {
//     $(".aa4").attr("id", "sel");
// } else if (url.indexOf("/news/") > -1||url.indexOf("/marketing/knowledge/") > -1||url.indexOf("/about/news/") > -1||url.indexOf("/website/newweb/") > -1||url.indexOf("/marketing/seo/") > -1) {
//     $(".aa5").attr("id", "sel");
// } else if (url.indexOf("/service/contact") > -1) {
//     $(".aa7").attr("id", "sel");
// } else if (url.indexOf("/about/") > -1||url.indexOf("/service/") > -1) {
//     $(".aa2").attr("id", "sel");
// } else if (url.indexOf("/mobile/") > -1||url.indexOf("/wangzhanjianshe/") > -1) {
//     $(".aa6").attr("id", "sel");
// } else if (url.indexOf("/website/") > -1) {
//     $(".aa3").attr("id", "sel");
// } else if (url.indexOf("/solutions/") > -1) {
//     $(".aa8").attr("id", "sel");
// } else if (url.indexOf("www.yibaixun.com/index.html") > -1) {
//     $(".aa1").attr("id", "sel");
// } else {
//     $(".aa1").attr("id", "sel");
// }
// #sel{border-bottom:3px solid #f3782a;color:#f3782a;}
// #sel:hover:after{opacity: 0;}

//控制导航鼠标放上去显示对应英文
// $(function(){
//     var li = $('.nav ul .m');
//     li.eq(0).find('a').eq(0).hover(function(){
//         $(this).html('HOME');
//     },function(){
//         $(this).html('首页');
//     });
//
//     li.eq(1).find('a').eq(0).hover(function(){
//         $(this).html('WEBSITE');
//     },function(){
//         $(this).html('网站建设');
//     });
//
//     li.eq(2).find('a').eq(0).hover(function(){
//         $(this).html('PRODUCT');
//     },function(){
//         $(this).html('产品服务');
//     });
//
//     li.eq(3).find('a').eq(0).hover(function(){
//         $(this).html('CASE');
//     },function(){
//         $(this).html('成功案例');
//     });
//
//     li.eq(4).find('a').eq(0).hover(function(){
//         $(this).html('SOLUTIONS');
//     },function(){
//         $(this).html('解决方案');
//     });
//
//     li.eq(5).find('a').eq(0).hover(function(){
//         $(this).html('NEWS');
//     },function(){
//         $(this).html('新闻动态');
//     });
//
//     li.eq(6).find('a').eq(0).hover(function(){
//         $(this).html('ABOUT');
//     },function(){
//         $(this).html('关于我们');
//     });
//
//     li.eq(7).find('a').eq(0).hover(function(){
//         $(this).html('CONTACT');
//     },function(){
//         $(this).html('联系我们');
//     });
//
// });


