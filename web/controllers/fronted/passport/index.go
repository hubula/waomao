package passport

import (
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
)

type IndexController struct {
	Ctx           iris.Context
	ServiceMember services.MemberService
}
