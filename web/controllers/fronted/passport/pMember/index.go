package pMember

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/models"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"strconv"
	"strings"
	"time"
)

type IndexController struct {
	Ctx           iris.Context
	ServiceMember services.MemberService
}

var (
	pathModels = "passport/member"
)

//Get http://localhost:8080/passport/member/login
func (c *IndexController) GetLogin() mvc.Result {
	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "login.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":       "登录 - 欢迎",
			"__COMEURL__": c.Ctx.URLParam("form"),
		},
		Layout: layout,
	}
}

//用户登录
func (c *IndexController) PostLoginin() {
	username := c.Ctx.PostValue("Username")
	password := c.Ctx.PostValue("Password")
	//验证
	adm, err := c.ServiceMember.Auth(username, password)

	form := c.Ctx.URLParam("form")
	if !strings.HasPrefix(form, conf.HomePage) {
		form = conf.HomePage
	}

	//错误检测
	if err != nil {
		//common.SessionSet(c.Ctx, "ISLOGIN", false)
		//c.Ctx.WriteString("账户登录失败，请重新尝试")
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"passport/member/login?form="+form)
		return
	} else {
		adm.Ip = common.ClientIP(c.Ctx.Request())
		common.SetLoginuser(c.Ctx.ResponseWriter(), adm)
		//c.Ctx.WriteString("账户登录成功 ")
		common.Redirect(c.Ctx.ResponseWriter(), form)
	}
}

// GetLoginwhich 退出
func (c *IndexController) GetLoginwhich() {
	common.SetLoginuser(c.Ctx.ResponseWriter(), nil)

	form := c.Ctx.URLParam("form")
	if !strings.HasPrefix(form, conf.HomePage) {
		form = conf.HomePage
	}

	common.Redirect(c.Ctx.ResponseWriter(), form)

}

//Get http://localhost:8080/passport/member/register
func (c *IndexController) GetRegister() mvc.Result {
	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "register.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":       "注册账号",
			"__COMEURL__": c.Ctx.URLParam("form"),
		},
		Layout: layout,
	}
}

//Get http://localhost:8080/passport/member/registernumber
func (c *IndexController) PostRegisternumber() {
	form := c.Ctx.URLParam("form")
	if !strings.HasPrefix(form, conf.HomePage) {
		form = conf.HomePage
	}

	if c.Ctx.PostValue("Mobile") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
		return
	}
	if c.Ctx.PostValue("Username") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
		return
	}
	if c.Ctx.PostValue("Password") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
		return
	}
	if c.Ctx.PostValue("Password") != c.Ctx.PostValue("Password2") {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
		return
	}
	if c.Ctx.PostValue("Mail") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
		return
	}
	if c.Ctx.PostValue("Salt") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
		return
	}

	user, err := c.ServiceMember.GetByMobile(c.Ctx.PostValue("Mobile"))
	if err == nil {
		if user.Uid != 0 {
			common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/register?form="+form)
			return
		}
	}

	// 将服务器UTC转成北京时间
	uTime := time.Now()
	regTime := uTime.In(time.FixedZone("CST", 8*60*60))

	password := common.PasswordSalt(c.Ctx.PostValue("Password"), c.Ctx.PostValue("Salt"))

	data := &models.Member{
		Number:   666,
		Mobile:   c.Ctx.PostValue("Mobile"),
		Username: c.Ctx.PostValue("Username"),
		Mail:     c.Ctx.PostValue("Mail"),
		Password: password,
		Salt:     c.Ctx.PostValue("Salt"),
		RegIp:    common.ClientIP(c.Ctx.Request()),
		RegTime:  regTime,
		IsDel:    0,
		GroupId:  0,
		TrueName: "",
		Name:     "",
	}

	_, err = c.ServiceMember.Create(data)

	//存入 Session
	sess := conf.Sess.Start(c.Ctx)

	if err != nil {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/registerresult?which=1010101010")
		return
	} else {
		// 成功导入数据库，还需要导入到缓存中

		//用户名
		common.SessionDel(c.Ctx, "zhucenumber")
		sess.Set("zhucenumber", strconv.Itoa(data.Number)+"1"+data.Mobile)

		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/passport/member/registerresult?which=0101010101")
		return
	}

}

// GetRegister http://localhost:8080/passport/member/registerresult?which=
func (c *IndexController) GetRegisterresult() mvc.Result {
	if !conf.AdminOpen {
		if c.Ctx.URLParam("which") != "1010101010" && c.Ctx.URLParam("which") != "0101010101" {
			return mvc.View{
				Name: "shared/error/" + common.RandErPaths(),
				Data: iris.Map{
					"Title": "404页面不见啦:( ",
				},
			}
		}
	}

	sess := conf.Sess.Start(c.Ctx)
	a := sess.GetString("zhucenumber")

	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "registerresult.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":       "注册账号",
			"zhucenumber": a,
			"zhuangtai":   c.Ctx.URLParam("which"),
		},
		Layout: layout,
	}
}
