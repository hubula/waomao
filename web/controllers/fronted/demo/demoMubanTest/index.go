package demoMubanTest

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

var (
	pathModels = "demo/mubantest"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

//Get http://localhost:8080/demo/mubantest
func (c *IndexController) Get() mvc.Result {
	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "index.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "模板演示",
		},
		Layout: layout,
	}
}

func (c *IndexController) GetCeshi() mvc.Result {
	return mvc.View{
		Name: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "muban.html"),
		Data: iris.Map{
			"Title": "测试模板路径是否正常",
		},
		Layout: common.MuBanLayout(c.Ctx.Request(), "", conf.MbIPath, pathModels),
	}
}

func (c *IndexController) GetIsmobile() mvc.Result {
	types := "电脑登录"
	if common.Mobile(c.Ctx.Request()) {
		types = "手机登录"
	}

	return mvc.View{
		Name: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "ismobile.html"),
		Data: iris.Map{
			"Title": "是否是手机等移动设备访问",
			"Types": types,
		},
		Layout: common.MuBanLayout(c.Ctx.Request(), "", conf.MbIPath, pathModels),
	}
}

func (c *IndexController) GetStyle1() mvc.Result {
	return mvc.View{
		Name: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "style1.html"),
		Data: iris.Map{
			"Title": "HTML模板使用方法一",
		},
		Layout: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "layout1.html"),
	}
}

func (c *IndexController) GetStyle2() mvc.Result {
	return mvc.View{
		Name: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "style2.html"),
		Data: iris.Map{
			"Title": "HTML模板使用方法二",
		},
		Layout: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "layout2.html"),
	}
}

func (c *IndexController) GetStyle3() mvc.Result {

	return mvc.View{
		Name: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "style3.html"),
		Data: iris.Map{
			"Title": "HTML模板使用方法",
		},
		Layout: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "layout.html"),
	}
}
