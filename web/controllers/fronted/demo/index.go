package demo

import (
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
)

var (
	pathModels = "demo"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

//Get http://localhost:8080/demo
//返回html页面
func (c *IndexController) Get() string {
	c.Ctx.Header("Content-Type", "text/html")
	return "welcome to Go+iris<br>" +
		"<a href='" + conf.HomePages + "'>网站首页</a><br>" +
		"<a href='" + conf.HomePages + pathModels + "'>本页面DEMO</a><hr>" +
		"<a href='" + conf.HomePages + pathModels + "/mubantest'>模板测试</a><br>" +
		"<a href='" + conf.HomePages + pathModels + "/mysqltest'>数据库测试</a><br>" +
		"<a href='" + conf.HomePages + pathModels + "/htmlorjq'>Html5和Jq调试</a><br>" +
		"<a href='" + conf.HomePages + pathModels + "/diytest'>自定义测试</a><hr>"
}
