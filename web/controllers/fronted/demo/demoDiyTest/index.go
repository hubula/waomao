package demoDiyTest

import (
	"fmt"
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	mobiledetect "github.com/Shaked/gomobiledetect"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"html/template"
)

var (
	pathModels = "demo/diytest"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

//Get http://localhost:8080/demo/diytest
func (c *IndexController) Get() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "index.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "自定义测试",
		},
	}
}

// GetJson 返回json数据
func (c *IndexController) GetJson() map[string]interface{} {
	rs := make(map[string]interface{})
	rs["code"] = 200
	rs["msg"] = "成功"
	return rs
}

// GetJson2 返回json数据
func (c *IndexController) GetJson2() mvc.Result {
	datalist := c.ServiceADemo.CountAll()
	return mvc.Response{
		Object: map[string]interface{}{
			"DataList": datalist,
		},
	}
}

// GetBoom 返回错误页面
func (c *IndexController) GetBoom() {
	c.Ctx.StatusCode(404)
	return
}

//MobileDetect请求头USER-AGENT
func (c *IndexController) GetAgent() {
	detect := mobiledetect.NewMobileDetect(c.Ctx.Request(), nil)
	fmt.Println(detect)
}

func (c *IndexController) GetQqdengji() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "dengji.html")

	dengji := 500
	dpic := common.QQLeveToSeveral(int64(dengji))
	b := common.QQLevePic(dpic)
	a := template.HTML(b)

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":   "QQ等级",
			"leixing": "QQ",
			"miaosu":  dengji,
			"Pc":      a,
		},
	}
}

func (c *IndexController) GetYydengji() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "dengji.html")

	dengji := 3071
	dpic := common.YYLeveToSeveral(int64(dengji))
	b := common.YYLevePic(dpic)
	a := template.HTML(b)
	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":   "YY等级",
			"leixing": "YY",
			"miaosu":  dengji,
			"Pc":      a,
		},
	}
}
