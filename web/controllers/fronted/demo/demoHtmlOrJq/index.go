package demoHtmlOrJq

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

var (
	pathModels = "demo/htmlorjq"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

//Get http://localhost:8080/demo/htmlorjq
func (c *IndexController) Get() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "index.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "请选择管理项目",
		},
	}
}

func (c *IndexController) GetV1() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "v1.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":     "请选择管理项目",
			"__theme__": "http://localhost:8080/public/",
			"mssage":    "当前路径Passport",
		},
	}
}

func (c *IndexController) GetV2() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "v2.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":     "请选择管理项目",
			"__theme__": "http://localhost:8080/public/",
			"mssage":    "当前路径Passport",
		},
	}
}

func (c *IndexController) GetTianjiahaoma() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "tianjiahaoma.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "养眼-添加号码",
		},
	}
}

func (c *IndexController) GetXuanze() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "xuanze.html")

	//<?php
	/**
	 *加入搜索条件
	 */
	//$where ="1";
	//$year_array = array(1=>'2015',2=>'2014');
	//$ctype_array = array(1=>'0',2=>'1');
	//$colors_array = array(1=>'0',2=>'1',3=>'2',4=>'3',5=>'4',6=>'5');
	//$lengths_array = array(1=>'0',2=>'1',3=>'2',4=>'3',5=>'4',6=>'5',7=>'6');
	//$micronaire_array = array(1=>'0',2=>'1',3=>'2',4=>'3',5=>'4');

	//if(isset($year)&&($year!=0)) $where .= " AND year=".$year_array[$year];
	//if(isset($ctype)&&($ctype!=0)) $where .= " AND ctype=".$ctype_array[$ctype];
	//if(isset($colors)&&($colors!=0)) $where .= " AND colors=".$colors_array[$colors];
	//if(isset($lengths)&&($lengths!=0)) $where .= " AND lengths=".$lengths_array[$lengths];
	//if(isset($micronaire)&&($micronaire!=0)) $where .= " AND micronaire=".$micronaire_array[$micronaire];
	/**
	 *加入搜索条件
	 */
	//?>

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "请选择管理yangyan项目",
		},
	}
}

func (c *IndexController) GetCaidan() mvc.Result {
	return mvc.View{
		Name: common.MuBanPath(c.Ctx.Request(), "", conf.MbIPath, pathModels, "caidan.html"),
		Data: iris.Map{
			"Title": "HTML模板使用方法一",
		},
	}
}
