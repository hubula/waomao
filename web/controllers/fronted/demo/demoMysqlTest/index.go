package demoMysqlTest

import (
	"fmt"
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/models"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"strconv"
	"time"
)

var (
	pathModels = "demo/mysqltest"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

//Get http://localhost:8080/demo/mysqltest
func (c *IndexController) Get() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "index.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "数据库操作",
		},
	}
}

//查询和插入演示
func (c *IndexController) GetChaandcha() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "chaandcha.html")
	//总数
	zongshu := c.ServiceADemo.CountAll()

	//所有数据
	page, _ := strconv.Atoi(c.Ctx.URLParam("page"))
	pageSize, _ := strconv.Atoi(c.Ctx.URLParam("pageSize"))

	//查询变量
	query := make(map[string]interface{})

	//定义sql
	sql := common.SqlWhere{
		TableName:   "",
		Conditions:  query,
		Fields:      []string{},
		OrderBy:     "to_id desc",
		Currentpage: int64(page),
		PageSize:    int64(pageSize),
		Uri:         c.Ctx.Request().RequestURI,
	}

	data, err := c.ServiceADemo.GetAll(&sql)
	//错误输出
	if err != nil {
		fmt.Println("err", err)
	}

	//--------------------------------------------------------------

	//根据Id查询
	byid, err := c.ServiceADemo.GetById(int64(8))
	if err != nil {
		fmt.Println(err)
	}

	//--------------------------------------------------------------

	//插入
	var sucNum string
	// 将服务器UTC转成北京时间
	uTime := time.Now()
	aa := uTime.In(time.FixedZone("CST", 8*60*60))

	data2 := &models.ADemo{
		ToId:          0,
		LoginName:     "777",
		Password:      "ooooooooo",
		Vsername:      "fgfgd",
		Mobile:        "14244755",
		Email:         "5555@qq.com",
		GenTime:       aa,
		LoginTime:     aa,
		LastLoginTime: aa,
		Count:         0,
		IsDel:         0,
	}

	_, err = c.ServiceADemo.Create(data2)
	if err != nil {
		sucNum = "失败"
		panic(err)
	} else {
		// 成功导入数据库，还需要导入到缓存中
		sucNum = "成功"
	}

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":   "插入和查询",
			"Zs":      zongshu,
			"pagings": data,
			"By":      byid,
			"sucNum":  sucNum,
		},
	}
}

//Get http://localhost:8080/demo/mysqltest/gaiandchu
//修改和删除演示
func (c *IndexController) GetGaiandchu() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "gaiandchu.html")

	//修改id 默认为1
	xiugaiid := 1

	if c.Ctx.URLParam("demoid") != "" {
		xiugaiid, _ = strconv.Atoi(c.Ctx.URLParam("demoid"))
	}

	//删除id 默认修改的+1
	shanchuid := xiugaiid + 1

	//修改
	var xiugai string
	_, err2 := c.ServiceADemo.Update(&models.ADemo{Id: int64(xiugaiid), Password: "18988", Email: "8888"}, nil)
	if err2 != nil {
		xiugai = "失败"
	} else {
		// 成功导入数据库，还需要导入到缓存中
		xiugai = "成功"
	}

	//删除
	var shanchu string
	_, err := c.ServiceADemo.Delete(int64(shanchuid))
	if err != nil {
		shanchu = "失败"
	} else {
		// 成功导入数据库，还需要导入到缓存中
		shanchu = "成功"
	}

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":     "删除和修改",
			"xiugai":    xiugai,
			"shanchu":   shanchu,
			"xiugaiid":  xiugaiid,
			"shanchuid": shanchuid,
		},
	}
}

//Get http://localhost:8080/demo/mysqltest/route
//打印路由的标签
func (c *IndexController) GetRoute() string {
	a := c.Ctx.Application().GetRoutesReadOnly()

	for _, only := range a {
		golog.Info(
			"name: %s, path:%s, method:%s",
			only.Name(), only.Path(), only.Method(),
		)
	}

	b := c.Ctx.Application().GetRouteReadOnly("返回html页面")
	fmt.Println(b.Name(), b.Path(), b.Method())

	c.Ctx.Header("Content-Type", "text/html")
	return "welcome to Go+iris <a href='" + conf.HttpOrHttps + conf.DomainName + "'>网站首页</a><br>"
}

//Get http://localhost:8080/demo/mysqltest/tianjia
//插入数据页面
func (c *IndexController) GetTianjia() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "tianjia.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "添加数据",
		},
	}
}

//Post http://localhost:8080/demo/mysqltest/tianjia
//插入数据处理页面
func (c *IndexController) PostTianjia() mvc.Result {
	// 将服务器UTC转成北京时间
	uTime := time.Now()
	aa := uTime.In(time.FixedZone("CST", 8*60*60))
	fmt.Print(c.Ctx.PostValue("GenTime"))

	data2 := &models.ADemo{
		ToId:          0,
		LoginName:     c.Ctx.PostValue("LoginName"),
		Password:      c.Ctx.PostValue("Password"),
		Vsername:      c.Ctx.PostValue("Vsername"),
		Mobile:        c.Ctx.PostValue("Mobile"),
		Email:         c.Ctx.PostValue("Email"),
		GenTime:       aa,
		LoginTime:     aa,
		LastLoginTime: aa,
		Count:         0,
		IsDel:         0,
	}

	var sucNum string

	_, err := c.ServiceADemo.Create(data2)
	if err != nil {
		sucNum = "失败"
		panic(err)
	} else {
		// 成功导入数据库，还需要导入到缓存中
		sucNum = "成功"
	}

	_, _ = c.Ctx.HTML(fmt.Sprintf("\n插入 %v，<a href='/demo/mysqltest/tianjia'>继续</a>", sucNum))
	return mvc.Response{
		Object: map[string]interface{}{
			"状态": sucNum,
		},
	}
}

// GetChaxun Get http://localhost:8080/demo/mysqltest/chaxun
//查询和插入演示
func (c *IndexController) GetChaxun() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "chaxun.html")

	a := c.Ctx.URLParam("LoginName")
	b := c.Ctx.URLParam("Vsername")
	e := c.Ctx.URLParam("Mobile")
	d := c.Ctx.URLParam("Email")
	page, _ := strconv.Atoi(c.Ctx.URLParam("page"))
	pageSize, _ := strconv.Atoi(c.Ctx.URLParam("pageSize"))

	//查询变量
	query := make(map[string]interface{})
	sousuoziduan := make(map[string]interface{})
	sousuoziduan["loginName"] = a
	sousuoziduan["vsername"] = b
	sousuoziduan["mobile"] = e
	sousuoziduan["email"] = d

	//query["ip=?"] = 1
	//query["is_del=?"] = 1
	//query["email REGEXP ? "] = 55

	if a != "" {
		query["login_name REGEXP ? "] = a
	}
	if b != "" {
		query["vsername REGEXP ? "] = b
	}
	if e != "" {
		query["mobile REGEXP ? "] = e
	}
	if d != "" {
		query["email REGEXP ? "] = d
	}

	//定义sql
	sql := common.SqlWhere{
		TableName:   "",
		Conditions:  query,
		Fields:      []string{},
		OrderBy:     "to_id desc",
		Currentpage: int64(page),
		PageSize:    int64(pageSize),
		Uri:         c.Ctx.Request().RequestURI,
	}

	data, err := c.ServiceADemo.GetAll(&sql)
	//错误输出
	if err != nil {
		fmt.Println("err", err)
		//c.Error(err.Error())
	}

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":   "条件查询",
			"pagings": data,
			"sousuo":  sousuoziduan,
		},
	}
}

//Get http://localhost:8080/demo/mysqltest/edit
//编辑页面
func (c *IndexController) GetEdit() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "giftEdit.html")

	id := c.Ctx.URLParamIntDefault("id", -1)

	//根据Id查询
	by, err := c.ServiceADemo.GetById(int64(id))
	if err != nil {
		fmt.Println(err)
	}

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "修改信息",
			"by":    by,
		},
	}
}

//Posthttp://localhost:8080/demo/mysqltest/cru
//提交修改
func (c *IndexController) PostCru() {
	visitor := models.ADemo{}
	err := c.Ctx.ReadForm(&visitor)
	if err != nil {
		c.Ctx.StatusCode(iris.StatusInternalServerError)
		_, _ = c.Ctx.WriteString("填入结构体失败" + err.Error())
	}

	//修改
	var xiugai string
	_, err2 := c.ServiceADemo.Update(&visitor, []string{"pwd", "username", "email"})
	if err2 != nil {
		xiugai = "失败"
	} else {
		// 成功导入数据库，还需要导入到缓存中
		xiugai = "成功"
	}
	fmt.Println("修改数据", xiugai)

	_, _ = c.Ctx.HTML(fmt.Sprintf("插入 %d %v，<a href='/demo/mysqltest/chaxun'>继续</a>", xiugai, visitor))
}

//Get http://localhost:8080/demo/mysqltest/ruandel
//软删除
func (c *IndexController) GetRuandel() {
	var shanchu string
	id := c.Ctx.URLParamIntDefault("id", -1)

	_, err := c.ServiceADemo.RuanDelete(int64(id))
	if err != nil {
		shanchu = "失败"
	} else {
		// 成功导入数据库，还需要导入到缓存中
		shanchu = "成功"
	}
	fmt.Println("软删除数据", shanchu, "id", id)

	_, _ = c.Ctx.HTML(fmt.Sprintf("软删除数据%v，<a href='/demo/mysqltest/chaxun'>继续</a>", shanchu))
}

//Get http://localhost:8080/demo/mysqltest/huishouzhan
//回收站
func (c *IndexController) GetHuishouzhan() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "huishouzhan.html")

	page, _ := strconv.Atoi(c.Ctx.URLParam("page"))
	pageSize, _ := strconv.Atoi(c.Ctx.URLParam("pageSize"))

	//查询变量
	query := make(map[string]interface{})
	query["is_del"] = "1"

	sql := common.SqlWhere{
		TableName:   "",
		Conditions:  query,
		Fields:      []string{},
		OrderBy:     "to_id desc",
		Currentpage: int64(page),
		PageSize:    int64(pageSize),
		Uri:         c.Ctx.Request().RequestURI,
	}

	data, err := c.ServiceADemo.GetAll(&sql)
	//错误输出
	if err != nil {
		fmt.Println("err", err)
	}

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":   "回收站",
			"pagings": data,
		},
	}
}

//Get http://localhost:8080/demo/mysqltest/del
//删除页面
func (c *IndexController) GetDel() {
	var shanchu string
	id := c.Ctx.URLParamIntDefault("id", -1)
	_, err := c.ServiceADemo.Delete(int64(id))
	if err != nil {
		shanchu = "失败"
	} else {
		// 成功导入数据库，还需要导入到缓存中
		shanchu = "成功"
	}
	fmt.Println("删除数据", shanchu)

	_, _ = c.Ctx.HTML(fmt.Sprintf("删除 %d，<a href='/demo/mysqltest/chaxun'>继续</a>", shanchu))
}

//Get http://localhost:8080/demo/mysqltest/del2
//Get 删除 直接跳转页面
func (c *IndexController) GetDel2() mvc.Result {
	_, _ = c.ServiceADemo.Delete(int64(4))
	return mvc.Response{
		Path: "/demo",
	}
}

//Get  http://localhost:8080/demo/mysqltest/shijian
func (c *IndexController) GetShijian() mvc.Result {
	pages, _ := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "shijian.html")

	//随机数
	suijishu := common.Random(9999)
	//获取时间戳 1626276224
	t := common.NowUnix()
	//时间格式化
	timeLayout := "2006-01-02 15:04:05"
	// 2020-05-01 00:00:00 +0000 UTC
	shoudong, _ := time.Parse(timeLayout, "2020-05-01 00:00:00")
	// 1588291200
	shoudongt := shoudong.Unix()
	//当前时间 2021-07-14 23:23:44
	shijian := common.FormatFromUnixTime(int64(t))

	miao := time.Now().Unix()
	haomiao := time.Now().UnixNano() / 1e6
	namiao := time.Now().UnixNano()
	natomiao := time.Now().UnixNano() / 1e9

	lingd := common.NextDayDuration()

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":     "demo",
			"suijishu":  suijishu,
			"t":         t,
			"shoudong":  shoudong,
			"shoudongt": shoudongt,
			"shijian":   shijian,
			"miao":      miao,
			"haomiao":   haomiao,
			"namiao":    namiao,
			"natomiao":  natomiao,
			"ling":      lingd,
		},
	}
}
