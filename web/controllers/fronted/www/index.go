package www

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

/**
 * 首页根目录的Controller
 * http://localhost:8080/
 */

var (
	pathModels = "www"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

//Get http://localhost:8080/
func (c *IndexController) Get() mvc.Result {
	//string([]rune(ctx.Path())[0:6]) == "/admin" ||
	//验证
	cookiess := common.GetLoginUser(c.Ctx.Request())
	if cookiess != nil {
		if common.NowUnix()-cookiess.Now > 10 {
			common.SetLoginuser(c.Ctx.ResponseWriter(), nil)
		}
	}

	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbIPath, pathModels, "index.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "首页",
			"cook":  cookiess,
		},
		Layout: layout,
	}
}
