package admin

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

var (
	pathModels = "admin"
)

//Get http://localhost:8080/admin
func (c *IndexController) Get() mvc.Result {
	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbAPath, pathModels, "index.html")

	v := c.Ctx.URLParam("heihua")
	if v != "zhenheyao" {
		return mvc.View{
			Name: "shared/error/1.html",
			Data: iris.Map{
				"Title": "访问的页面不存在",
			},
		}
	}

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "后台管理",
		},
		Layout: layout,
	}
}
