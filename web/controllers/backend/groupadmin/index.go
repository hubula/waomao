package groupadmin

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"gitee.com/hubula/waomao/web/viewsmodels"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

var (
	pathModels = "groupadmin"
)

//Get http://localhost:8080/groupadmin
func (c *IndexController) Get() mvc.Result {
	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbAPath, pathModels, "index.html")

	urlpath := c.Ctx.Request().URL.String()

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":    "后台管理",
			"__Form__": urlpath,
			"hhua":     "gan" + conf.GroupadminKey + conf.GroupadminVal,
		},
		Layout: layout,
	}
}

func (c *IndexController) PostTologin() {
	sess := conf.Sess.Start(c.Ctx)
	cant := c.Ctx.PostValue("Cant")
	//开发阶段判断,如果为 dev true 直接赋值 admin用户的session ，方便开发
	if conf.AdminOpen {
		//设置Session
		adm1 := &viewsmodels.AdminSession{
			Uid:      88,
			Username: "guanliyuan",
			Now:      0,
			Ip:       "",
			Sign:     "",
		}
		common.LoginSessionSet(c.Ctx, "aadmin_cantkey", adm1)

		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/groupadmin/manager?form=/groupadmin/manager/member/ltom")
		return
	}

	if len(cant) > 11 {
		if cant == "189881212222" {
			sess.Set("tologin_ck", string("true"))
			common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/groupadmin/manager/member/login?cant=gopditlgnrmgan"+conf.GroupadminKey+conf.GroupadminVal)
		} else {
			common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+c.Ctx.URLParam("form"))
		}
	} else {
		if cant == "18988" {
			sess.Set("tologin_ck", string("true"))
			common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/groupadmin/manager/member/login?cant=gopditlgnrmgan"+conf.GroupadminKey+conf.GroupadminVal)
		} else {
			common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+c.Ctx.URLParam("form"))
		}
	}
}
