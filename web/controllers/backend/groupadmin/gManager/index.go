package gManager

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/services"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceADemo services.ADemoService
}

var (
	pathModels = "groupadmin/manager"
)

func (c *IndexController) Get() mvc.Result {
	if !conf.AdminOpen {
		if c.Ctx.URLParam("form") != "rpiaarblom"+conf.GroupadminVal+"666" {
			return mvc.View{
				Name: "shared/error/" + common.RandErPaths(),
				Data: iris.Map{
					"Title": "404页面不见啦:( ",
				},
			}
		}
	}

	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbAPath, pathModels, "index.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "后台管理",
		},
		Layout: layout,
	}
}
