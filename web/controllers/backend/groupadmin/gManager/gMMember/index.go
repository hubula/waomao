package gMMember

import (
	"encoding/json"
	"fmt"
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/models"
	"gitee.com/hubula/waomao/services"
	"gitee.com/hubula/waomao/web/viewsmodels"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"time"
)

type IndexController struct {
	Ctx          iris.Context
	ServiceAdmin services.AdminService
}

var (
	pathModels = "groupadmin/manager/member"
)

// GetLogin http://localhost:8080/groupadmin/manager/member/login
func (c *IndexController) GetLogin() mvc.Result {
	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbAPath, pathModels, "login.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":    "登录 - 欢迎",
			"__Form__": "gudnneeeontagergan" + conf.GroupadminKey + conf.GroupadminVal,
		},
		Layout: layout,
	}
}

// PostLtom 登录验证
func (c *IndexController) PostLtom() {
	username := c.Ctx.PostValue("Username")
	password := c.Ctx.PostValue("Password")
	//验证
	adm, err := c.ServiceAdmin.Auth(username, password)

	if err != nil {
		//c.Ctx.WriteString("账户登录失败，请重新尝试")
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/groupadmin/manager/member/login?cant=gopditlgnrmgan"+conf.GroupadminKey+conf.GroupadminVal)
	} else {
		//设置Session
		common.LoginSessionSet(c.Ctx, "aadmin_cantkey", adm)
		common.SessionDel(c.Ctx, "tologin_ck")
		//c.Ctx.WriteString("账户登录成功 ")
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePage+"/groupadmin/manager?form=rpiaarblom"+conf.GroupadminVal+"666")
	}
}

// PostAuthlogic 退出
func (c *IndexController) PostAuthlogic() {
	if !conf.AdminOpen {
		if c.Ctx.URLParam("which") != "Login" {
			c.Ctx.StatusCode(404)
			return
		}
	}
	common.SessionDel(c.Ctx, "aadmin_cantkey")
	common.SessionDel(c.Ctx, "ISLOGIN")
	//common.SetLoginuser(c.Ctx.ResponseWriter(), nil)
	common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages)
}

// GetRegister http://localhost:8080/groupadmin/manager/member/register
func (c *IndexController) GetRegister() mvc.Result {
	_, user, _ := common.LoginSessionGet(c.Ctx, "aadmin_cantkey")
	adminsession, _ := c.ServiceAdmin.GetById(int64(user.Uid))

	if !conf.AdminOpen {
		if c.Ctx.URLParam("which") != "6" || adminsession.Anumber != 18988 {
			return mvc.View{
				Name: "shared/error/" + common.RandErPaths(),
				Data: iris.Map{
					"Title": "404页面不见啦:( ",
				},
			}
		}
	}

	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbAPath, pathModels, "register.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title": "注册账号",
		},
		Layout: layout,
	}
}

//Get http://localhost:8080/passport/member/register
func (c *IndexController) PostRegisterhappy() {
	_, user, _ := common.LoginSessionGet(c.Ctx, "aadmin_cantkey")
	adminsession, _ := c.ServiceAdmin.GetById(int64(user.Uid))

	if !conf.AdminOpen {
		if c.Ctx.URLParam("which") != "99" || adminsession.Anumber != 18988 {
			c.Ctx.StatusCode(404)
			return
		}
	}

	// 将服务器UTC转成北京时间
	uTime := time.Now()
	regTime := uTime.In(time.FixedZone("CST", 8*60*60))

	if c.Ctx.PostValue("Mobile") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/register?which=6")
		return
	}
	if c.Ctx.PostValue("Username") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/register?which=6")
		return
	}
	if c.Ctx.PostValue("Mail") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/register?which=6")
		return
	}
	if c.Ctx.PostValue("Password") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/register?which=6")
		return
	}
	if c.Ctx.PostValue("Password") != c.Ctx.PostValue("Password2") {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/register?which=6")
		return
	}
	if c.Ctx.PostValue("Salt") == "" {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/register?which=6")
		return
	}

	password := common.PasswordSalt(c.Ctx.PostValue("Password"), c.Ctx.PostValue("Salt"))

	data2 := &models.Admin{
		Username:   c.Ctx.PostValue("Username"),
		Password:   password,
		Mail:       c.Ctx.PostValue("Mail"),
		Salt:       c.Ctx.PostValue("Salt"),
		TimeAdd:    regTime,
		TimeUpdate: regTime,
		Ip:         common.ClientIP(c.Ctx.Request()),
		JobNo:      "",
		NickName:   "",
		TrueName:   "",
		Qq:         "",
		Phone:      "",
		Mobile:     c.Ctx.PostValue("Mobile"),
		IsDel:      0,
	}

	_, err := c.ServiceAdmin.Create(data2)

	ssssession := common.ConvertSession(data2)
	//存入 Session
	sess := conf.Sess.Start(c.Ctx)
	//结构体序列化为json,为存入 Session做准备
	str2, err := json.Marshal(ssssession)
	if err != nil {
		fmt.Println("session序列化为json失败:" + err.Error())
		//c.Error(err.Error())
		return
	}

	//用户名
	common.SessionDel(c.Ctx, "zhucenumber")
	sess.Set("zhucenumber", string(str2))

	if err != nil {
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/registerresult?which=1010101010")
		return
	} else {
		// 成功导入数据库，还需要导入到缓存中
		common.Redirect(c.Ctx.ResponseWriter(), conf.HomePages+"groupadmin/manager/member/registerresult?which=0101010101")
		return
	}
}

// GetRegister http://localhost:8080/groupadmin/manager/member/registerresult
func (c *IndexController) GetRegisterresult() mvc.Result {
	_, user, _ := common.LoginSessionGet(c.Ctx, "aadmin_cantkey")
	adminsession, _ := c.ServiceAdmin.GetById(int64(user.Uid))

	if !conf.AdminOpen {
		if adminsession.Anumber != 18988 {
			return mvc.View{
				Name: "shared/error/" + common.RandErPaths(),
				Data: iris.Map{
					"Title": "404页面不见啦:( ",
				},
			}
		}

		if c.Ctx.URLParam("which") != "1010101010" && c.Ctx.URLParam("which") != "0101010101" {
			return mvc.View{
				Name: "shared/error/" + common.RandErPaths(),
				Data: iris.Map{
					"Title": "404页面不见啦:( ",
				},
			}
		}
	}

	sess := conf.Sess.Start(c.Ctx)
	a := sess.GetString("zhucenumber")
	//初始化
	stu := &viewsmodels.AdminSession{}
	//字符串 转换为 session 结构体并赋值
	err := json.Unmarshal([]byte(a), &stu)
	if err != nil {
	}

	pages, layout := common.MuBanPaths(c.Ctx.Request(), "", conf.MbAPath, pathModels, "registerresult.html")

	return mvc.View{
		Name: pages,
		Data: iris.Map{
			"Title":       "注册账号",
			"zhucenumber": stu,
			"zhuangtai":   c.Ctx.URLParam("which"),
		},
		Layout: layout,
	}
}
