package viewsmodels

//前台cookie
type MemberCookie struct {
	Uid      int
	Umember  string
	Username string
	Now      int
	Ip       string
	//签名sign
	Sign string
}
