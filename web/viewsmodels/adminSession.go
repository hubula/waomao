package viewsmodels

//后台session
type AdminSession struct {
	Uid      int
	Umeber   string
	Username string
	Now      int
	Ip       string
	//签名sign
	Sign string
}
