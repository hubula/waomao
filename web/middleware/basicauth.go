package middleware

import (
	_ "github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/basicauth"
)

//用户名 密码
var BasicAuth = basicauth.New(basicauth.Config{
	Users: map[string]string{
		"admin": "123456",
	},
})
