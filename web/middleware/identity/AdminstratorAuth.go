package identity

import (
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/conf"
	"github.com/kataras/iris/v12"
)

func AdminAuth(ctx iris.Context) {
	groupadminCant := "gan" + conf.GroupadminKey + conf.GroupadminVal
	gTologinCant := "gopditlgnrm" + groupadminCant
	lTomCant := "gudnneeeontager" + groupadminCant

	//if ctx.Method() == "GET" {
	//
	//}
	//fmt.Println(ctx.Request().URL.String())

	switch ctx.Path() {
	case "/groupadmin":
		//开发阶段判断,如果为 dev true 直接跳过 方便开发
		if conf.AdminOpen {
			ctx.Next()
			return
		} else {
			if ctx.URLParam(conf.GroupadminKey) == conf.GroupadminVal {
				ctx.Next()
				return
			} else {
				ctx.StatusCode(404)
				return
			}
		}
	case "/groupadmin/tologin":
		if conf.AdminOpen {
			ctx.Next()
			return
		} else {
			if ctx.URLParam("hhua") == groupadminCant && ctx.URLParam("form") == "/groupadmin?"+conf.GroupadminKey+"="+conf.GroupadminVal {
				ctx.Next()
				return
			} else {
				ctx.StatusCode(404)
				return
			}
		}
	case "/groupadmin/manager/member/login":
		if conf.AdminOpen {
			ctx.Next()
			return
		} else {
			if ctx.URLParam("cant") == gTologinCant {
				isLogin := conf.Sess.Start(ctx).GetString("tologin_ck")
				if isLogin != "true" {
					//fmt.Println("账户未登录,请先登录")
					ctx.StatusCode(404)
					return
				}
				ctx.Next()
				return
			} else {
				ctx.StatusCode(404)
				return
			}
		}
	case "/groupadmin/manager/member/ltom":
		if conf.AdminOpen {
			ctx.Next()
			return
		} else {
			if ctx.URLParam("form") == lTomCant {
				ctx.Next()
				return
			} else {
				ctx.StatusCode(404)
				return
			}
		}

	}

	//检查登录状态
	isLogin, adminsessions, err := common.LoginSessionGet(ctx, "aadmin_cantkey")

	if err != nil {
		//fmt.Println("账户未登录,请先登录")
		ctx.StatusCode(404)
		return
	}

	if !isLogin {
		//app.Logger().Info(" 账户未登录 ")
		//fmt.Println("账户未登录")
		ctx.StatusCode(404)
		return
	}
	//app.Logger().Info(" 账户已登录 ")

	//写入中间件
	ctx.Values().Set("loginname", adminsessions)
	//需要时候取出来
	//user := ctx.Values().Get("loginname")
	ctx.ViewData("loginname", adminsessions)

	ctx.Next()
	return
}
