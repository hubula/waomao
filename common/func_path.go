package common

import (
	"net/http"
	"os"
	"strings"
)

/*PathExists 判断文件或文件夹是否存在 存在为true
  如果返回的错误为nil,说明文件或文件夹存在
  如果返回的错误类型使用os.IsNotExist()判断为true,说明文件或文件夹不存在
                   使用os.IsExist()判断为true,说明文件或文件夹存在
  如果返回的错误为其它类型,则不确定是否在存在
*/
func PathExists(path string) bool {
	//os.Stat获取文件信息
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			//fmt.Printf("common->path_file : line 20 error![%v]\n", err)
			return true
		}
		//fmt.Printf("common->path_file : line 23 error![%v]\n", err)
		return false
	}
	return true
}

// IsDir 判断所给路径是否为文件夹 tf = isdir('A')
//如果A是一个文件夹，返回逻辑1（true），否则返回0（false）
func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

// IsFile 判断所给路径是否为文件
//func IsFile(path string) bool {
//	return !IsDir(path)
//}

func MuBanPath(r *http.Request, cookieMobile string, indexOrAdmin string, pathModel string, page string) string {
	path := ViewsPathCookie(r, cookieMobile, indexOrAdmin, pathModel) + page
	//检查字符串是否存在 使用Contains()函数
	if strings.Contains(path, "wap") {
		if PathExists(path) {
			return path
		}
		return ViewsPathCookie(r, "pc", indexOrAdmin, pathModel) + page

	}
	return path
}

func MuBanLayout(r *http.Request, cookieMobile string, indexOrAdmin string, pathModel string) string {
	path := ViewsPathCookie(r, cookieMobile, indexOrAdmin, pathModel) + "layout.html"
	//检查字符串是否存在 使用Contains()函数
	if strings.Contains(path, "wap") {
		if PathExists(path) {
			return path
		}
		return ViewsPathCookie(r, "pc", indexOrAdmin, pathModel) + "layout.html"

	}
	return path
}

func MuBanPaths(r *http.Request, cookieMobile string, indexOrAdmin string, pathModel string, page string) (pages string, layout string) {
	path := ViewsPathCookie(r, cookieMobile, indexOrAdmin, pathModel)
	pages = path + page
	//检查字符串是否存在 使用Contains()函数
	if strings.Contains(pages, "wap") {
		if !PathExists(pages) {
			path = ViewsPathCookie(r, "pc", indexOrAdmin, pathModel)
		}
	}
	pages = path + page
	layout = path + "layout.html"
	return pages, layout
}
