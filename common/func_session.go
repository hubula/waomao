package common

import (
	"encoding/json"
	"fmt"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/web/viewsmodels"
	"github.com/kataras/iris/v12"
)

// LoginSessionSet 用户登录 session 填充
func LoginSessionSet(c iris.Context, sessionname string, sessionvalue *viewsmodels.AdminSession) {
	sess := conf.Sess.Start(c)
	//结构体序列化为json,为存入 Session做准备
	str2, err := json.Marshal(sessionvalue)
	if err != nil {
		fmt.Println("session序列化为json失败:" + err.Error())
		return
	}

	//用户名
	sess.Set(sessionname, string(str2))
	//登录状态
	sess.Set("ISLOGIN", true)

	//格式化显示
	//buff,_ := json.MarshalIndent(sessionvalue,"","   ")
	//fmt.Println("str => ?", string(buff))
}

// LoginSessionGet 获取
func LoginSessionGet(c iris.Context, sessionname string) (bool, *viewsmodels.AdminSession, error) {
	sess := conf.Sess.Start(c)
	login, err := sess.GetBoolean("ISLOGIN")
	//获取值为SESSION_NAME 的session，并转换为字符串型
	user := sess.GetString(sessionname)
	//fmt.Println("session获取打印:",usern)

	if err != nil {
		return false, nil, fmt.Errorf("session 不存在")
	}

	if err == nil && user == "" {
		return false, nil, fmt.Errorf("session 为空")
	}

	//初始化
	stu := &viewsmodels.AdminSession{}
	//字符串 转换为 session 结构体并赋值
	err = json.Unmarshal([]byte(user), &stu)
	if err != nil {
		return false, nil, fmt.Errorf("session 序列号转换错误" + err.Error())
	}
	return login, stu, nil
}

// SessionDel 删除
func SessionDel(c iris.Context, sessionname string) {
	sess := conf.Sess.Start(c)
	sess.Delete(sessionname)
}
