package common

import (
	"fmt"
	"math/rand"
	"time"
)

func RandErPaths() string {
	message := fmt.Sprint(randomFormat())
	return message
}

//设置初始值
func init() {
	rand.Seed(time.Now().UnixNano())
}

// 随机选择
func randomFormat() string {
	formats := []string{
		"1.html",
		"2.html",
		"3.html",
		"4.html",
		"5.html",
	}

	//通过指定 格式切片的随机索引
	return formats[rand.Intn(len(formats))]
}
