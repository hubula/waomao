package common

import (
	"gitee.com/hubula/waomao/conf"
	mobiledetect "github.com/Shaked/gomobiledetect"
	"net/http"
)

func Mobile(r *http.Request) bool {
	detect := mobiledetect.NewMobileDetect(r, nil)
	if detect.IsMobile() || detect.IsTablet() {
		if detect.IsMobile() && detect.IsTablet() {
			//fmt.Println("Hello, this is Tablet")
		} else {
			//fmt.Println("Hello, this is Mobile")
		}
		return true
	} else {
		//fmt.Println("Hello, this is Desktop")
		return false
	}
}

// http.Request cookie设备信息 前台或者后台 模块路径
func ViewsPathCookie(r *http.Request, cookieMobile string, indexOrAdmin string, pathModel string) string {
	if cookieMobile == conf.WapPath || cookieMobile == conf.PcPath {
		if indexOrAdmin == conf.MbAPath || indexOrAdmin == conf.MbIPath {
			return indexOrAdmin + "/" + cookieMobile + "/" + pathModel + "/"
		}
		return conf.MbIPath + "/" + cookieMobile + "/" + pathModel + "/"
	}

	if !(indexOrAdmin == conf.MbAPath || indexOrAdmin == conf.MbIPath) {
		indexOrAdmin = conf.MbIPath
	}
	detect := mobiledetect.NewMobileDetect(r, nil)
	if detect.IsMobile() || detect.IsTablet() {
		if detect.IsMobile() && detect.IsTablet() {
			//fmt.Println("Hello, this is Tablet")
		} else {
			//fmt.Println("Hello, this is Mobile")
		}
		return indexOrAdmin + "/" + conf.WapPath + "/" + pathModel + "/"
	} else {
		//fmt.Println("Hello, this is Desktop")
		return indexOrAdmin + "/" + conf.PcPath + "/" + pathModel + "/"
	}
}
