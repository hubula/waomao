package common

import (
	"crypto/md5"
	"fmt"
	"gitee.com/hubula/waomao/conf"
	"gitee.com/hubula/waomao/models"
	"gitee.com/hubula/waomao/web/viewsmodels"
	"net"
	"net/http"
	"strconv"
	"strings"
)

//加密密码
func PasswordSalt(pass, salt string) string {
	//密码干扰码
	str := conf.PassWordSalt + pass + salt + conf.AesKey
	//return crypt.Md5(crypt.Sha256(str))
	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

//验证
func PasswordVerify(password, pass, salt string) bool {
	//strings.EqualFold(password, PasswordSalt(pass, salt))
	return password == PasswordSalt(pass, salt)
}

// ConvertSession 转换
func ConvertSession(admUser *models.Admin) *viewsmodels.AdminSession {
	//赋值
	Session := &viewsmodels.AdminSession{
		Uid:      admUser.Aid,
		Umeber:   strconv.Itoa(admUser.Anumber),
		Username: admUser.Username,
		Now:      NowUnix(),
		Ip:       admUser.Ip,
		Sign:     "",
	}
	return Session
}

// ConvertCookie 转换
func ConvertCookie(admUser *models.Member) *viewsmodels.MemberCookie {
	//赋值
	Session := &viewsmodels.MemberCookie{
		Uid:      admUser.Uid,
		Umember:  strconv.Itoa(admUser.Number),
		Username: admUser.Username,
		Now:      NowUnix(),
		Ip:       admUser.RegIp,
		Sign:     "",
	}
	return Session
}

//ClientIP 得到客户端IP地址
func ClientIP(request *http.Request) string {
	host, _, _ := net.SplitHostPort(request.RemoteAddr)
	return host
}

//Redirect 跳转URL
func Redirect(writer http.ResponseWriter, url string) {
	writer.Header().Add("Location", url)
	writer.WriteHeader(http.StatusFound)
}

//获取 url http://localhost:8080/passport/member/login?dfad=dfas
func GetURL(r *http.Request) (Url string) {
	scheme := "http://"
	if r.TLS != nil {
		scheme = "https://"
	}
	return strings.Join([]string{scheme, r.Host, r.RequestURI}, "")
}

//获取 url
//func ReturnUrl(r *http.Request) *viewsmodels.ReturnUrl {
//	scheme := "http://"
//	if r.TLS != nil {
//		scheme = "https://"
//	}
//
//	url := &viewsmodels.ReturnUrl{
//		HttpOrHttps: conf.HttpOrHttps,
//		DomainName:  conf.DomainName,
//		Path:        r.URL.Path,
//		UrlHost:     scheme + r.Host,
//		UrlPath:     scheme + r.Host + r.URL.Path,
//		URL:         scheme + r.Host + r.RequestURI,
//	}
//
//	return url
//}
