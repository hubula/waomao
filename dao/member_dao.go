package dao

import (
	"fmt"
	"gitee.com/hubula/waomao/common"
	"gitee.com/hubula/waomao/datasource"
	"gitee.com/hubula/waomao/models"
	"github.com/xormplus/xorm"
	"html/template"
)

type MemberDao struct {
	//数据库相关的操作 xorm引擎
	engine *xorm.Engine
}

//New Dao 实例化公共方法
func NewMemberDao(engine *xorm.Engine) *MemberDao {
	return &MemberDao{
		engine: engine,
	}
}

// NewAdmin 初始化
func (d *MemberDao) newModel() *models.Member {
	return new(models.Member)
}

// newMakeDataArr 初始化列表
func (d *MemberDao) newMakeDataArr() []models.Member {
	return make([]models.Member, 0)
}

// GetAll 列表查询
//条件 fields字段常和更新一起使用为0查询或更新所有字段 排序 页数 每页条数 返回分页内容 err
//q, fields, orderBy, page, limit) (*Paginator, error)
func (d *MemberDao) GetAll(sqlwhere *common.SqlWhere) (*common.SqlReturn, error) {
	//获取符合条件的数据总数
	sessionCount := datasource.Filter(sqlwhere.Conditions)
	defer sessionCount.Close()
	count, err := sessionCount.Count(&models.ADemo{})
	if err != nil {
		fmt.Println(err)
		return nil, common.NewError(err.Error())
	}
	if count == 0 {
		return nil, nil
	}

	//返回 总页数
	po := common.GetPages(sqlwhere, count)
	str := common.H(po)

	//获取数据
	session := datasource.Filter(sqlwhere.Conditions)
	defer session.Close()
	if sqlwhere.OrderBy != "" {
		session.OrderBy(sqlwhere.OrderBy)
	}
	session.Limit(int(po.PageSize), int((po.Currentpage-1)*sqlwhere.PageSize))
	if len(sqlwhere.Fields) == 0 {
		//更新所有字段
		session.AllCols()
	}

	data := d.newMakeDataArr()

	err = session.Find(&data)
	if err != nil {
		fmt.Println(err)
		return nil, common.NewError(err.Error())
	}

	//整合需要输出的内容
	sqlR := new(common.SqlReturn)

	sqlR.Data = make([]interface{}, len(data))
	for y, x := range data {
		sqlR.Data[y] = x
	}

	sqlR.Str = template.HTML(str)
	sqlR.Page = po.Currentpage
	sqlR.PageSize = po.PageSize
	sqlR.TotalCount = count
	sqlR.TotalPage = po.TotalPage
	sqlR.Href = po.Href
	return sqlR, nil
}

// GetById 获取单条记录
func (d *MemberDao) GetById(id int64) (*models.Member, error) {
	m := d.newModel()
	//fmt.Println(id)
	m.Uid = int(id)

	s, err := d.engine.Get(m)
	if err == nil {
		if s {
			return m, nil
		}
		return nil, common.NewError("不存在")
	}
	return nil, err
}

func (d *MemberDao) GetByUnumber(id int64) (*models.Member, error) {
	m := d.newModel()
	//fmt.Println(id)
	m.Number = int(id)

	s, err := d.engine.Get(m)
	if err == nil {
		if s {
			return m, nil
		}
		return nil, common.NewError("不存在")
	}
	return nil, err
}

func (d *MemberDao) GetByMobile(mobile string) (*models.Member, error) {
	m := d.newModel()
	//fmt.Println(id)
	m.Mobile = mobile

	s, err := d.engine.Get(m)
	if err == nil {
		if s {
			return m, nil
		}
		return nil, common.NewError("不存在")
	}
	return nil, err
}

// CountAll 统计
func (d *MemberDao) CountAll() int64 {
	m := d.newModel()
	num, err := d.engine.Count(m)
	if err != nil {
		return 0
	} else {
		return num
	}
}

// Create 添加单条记录
func (d *MemberDao) Create(data *models.Member) (int64, error) {
	num, err := d.engine.InsertOne(data)
	return num, err
}

// Update 修改单条记录
func (d *MemberDao) Update(data *models.Member, columns []string) (int64, error) {
	num, err := d.engine.ID(data.Uid).MustCols(columns...).Update(data)
	return num, err
}

// RuanDelete 软删除单条记录
func (d *MemberDao) RuanDelete(id int64) (int64, error) {
	m := d.newModel()
	m.Uid = int(id)
	m.IsDel = 1

	num, err := d.engine.ID(m.Uid).Update(m)
	if err == nil {
		return num, nil
	}
	return num, err
}

// Delete 删除单条记录
func (d *MemberDao) Delete(id int64) (int64, error) {
	m := d.newModel()
	m.Uid = int(id)

	num, err := d.engine.Delete(m)
	if err == nil {
		return num, nil
	}
	return num, err
}

// GetWhere Sql语句
//ll := "12"
//sql := "SELECT * FROM `lianxiren` WHERE `tel` LIKE '%" + ll + "%' "
//where := c.ServiceADemo.GetWhere(sql)
func (d *MemberDao) GetWhere(sql string) []models.Member {
	datalist := d.newMakeDataArr()
	err := d.engine.SQL(sql).Find(&datalist)
	if err != nil {
		return datalist
	} else {
		return datalist
	}
}
